# Flow-induced instabilities of springs-mounted plates in viscous flows: a global stability approach #

> Codes for performing the linear stability analysis of heaving and pitching spring-mounted rigid objects in open
viscous flows. We use the FreeFEM language for the finite element discretization of the Navier--Stokes equations and the PETSc/SLEPc libraries for solving sparse linear systems and eigenproblems.

![header](fluttermode.png)

The codes available in this repository allow reproducing the results from the following paper:
```
@article{moulin2021lsa,
    Author = {Moulin, Johann and Marquet, Olivier},
    Title = {{Flow-induced instabilities of springs-mounted plates in viscous flows: A global stability approach}},
    Year = {2021},
    Volume = {33},
    number = {3},
    Pages = {034133},
    Journal = {Physics of Fluids},
    Url = {https://bitbucket.org/jmoulin/pof2021},
    doi = {10.1063/5.0038368}
}
```

## Getting started
### Dependencies
Make sure you have access to a recent [FreeFEM](https://freefem.org/) installation (version 4.3 or above), compiled with the `PETSc` and `PETSc-complex` **(with SLEPc)** plugins.
An introduction to FreeFEM may be found in this [tutorial](http://jolivet.perso.enseeiht.fr/FreeFem-tutorial/#pf1c). The user is reported to the [PETSc](https://www.mcs.anl.gov/petsc/petsc-current/docs/manual.pdf) and [SLEPc](http://slepc.upv.es/documentation/slepc.pdf) manuals for more details about the options used in the solvers.

### Usage example
We show here how to reproduce the linear stability spectrum of Fig. 3 in the
companion paper.
First, a steady solution of the fluid-solid equations must be computed with:
```bash
FreeFem++-mpi -wg Baseflow2DOF.edp # the -wg option enables graphics
```
The computed solution is stored in the `BASEFLOWS` directory. Note that the MPI version of FreeFEM is used here only to access its PETSc/SLEPc interfaces, but all codes in this repertory are _sequential only_.

Second, the linear stability problem can be assessed with:
```bash
FreeFem++-mpi -wg Eigensolver2DOF.edp
```
The above command will extract the 10 eigenvalues closest to the requested shift in the complex plane. In order to compute a larger portion of the total spectrum, on can continue sweeping along the imaginary axis by changing `-shift_imag`. The eigenvalues and eigenvectors are stored in the `DIREIG` directory.

### More options ...
Non-dimensional numbers (see definitions in the paper)

* `-Re` (default to `2900`): Reynolds number
* `-m` (default to `1000`): solid-to-fluid mass ratio
* `-U` (default to `4.7`): reduced velocity

Eigensolver numerical parameters

* `-nev` (default to `10`): requested number of eigenvalues
* `-shift_real` (default to `0.1`): real part of the shift
* `-shift_imag` (default to `1/U`): imaginary part of the shift. Note: the default value allows targeting the location of the solid natural frequencies.

### Mathematical details
The mathematical details regarding the finite element formulation used to discretized the incompressible Navier--Stokes equations may be found in section 1.2 of the first author's [PhD thesis](https://w3.onera.fr/erc-aeroflex/sites/w3.onera.fr.erc-aeroflex/files/docs/johann/Moulin-PhDthesis-BeforeDefense.pdf).

## Acknowledgements
* [European Research Council](https://erc.europa.eu/) (ERC), project [AEROFLEX](https://w3.onera.fr/erc-aeroflex/home), grant number 638307.